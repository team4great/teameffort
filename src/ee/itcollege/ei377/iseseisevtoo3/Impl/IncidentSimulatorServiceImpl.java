package ee.itcollege.ei377.iseseisevtoo3.Impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import ee.itcollege.ei377.iseseisevtoo3.IncidentService;
import ee.itcollege.ei377.iseseisevtoo3.models.Incident;

public class IncidentSimulatorServiceImpl implements IncidentService{
	
	
	@Override
	public List<Incident> getUnresolvedIncidents(){

		 List<Incident> incidents = new ArrayList<Incident>();
		 Incident temp = new Incident();
		 for(int i = 1; i < 11; i++){
  
	     Calendar cal = Calendar.getInstance();  
	     cal.setTime(new Date());  
	     cal.add(Calendar.DATE, i); // add i days  
	     
	     if (i % 2 == 0)
	     {
	    	 temp.setDescription("Purjus tudeng jooksis vastu posti");
	    	 temp.setLocation("Tartu");
	    	 temp.setStatus("lahendatud");
	     }
	     else{
	    	 temp.setDescription("P�der l�i kabjaga venelast");
	    	 temp.setLocation("Narva");
	    	 temp.setStatus("lahendamata");	    	 	 
	     }
		
		 temp.setEndDate(cal.getTime());
		 temp.setInvolvedGuardCount(5);
		 temp.setStartDate(new Date());
		 
		 incidents.add(temp);
		 
		 }
		 
		 return incidents;
	}
	
	public IncidentSimulatorServiceImpl(){	
	}
	
}
