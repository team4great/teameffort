package ee.itcollege.ei377.iseseisevtoo3.Controllers;

import java.util.Collection;
import java.util.List;
import java.util.Map;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import ee.itcollege.ei377.iseseisevtoo3.models.Incident;

@Controller
@SessionAttributes("incidentSession")
@RequestMapping("/incident")
public class IncidentController {
	
	
	
		
	@RequestMapping(method = RequestMethod.GET, value="/listAll")
	public List<Incident> getAll(){
		Incident incident = new Incident();
		return incident.getGetAllIncidents();
	}
	
	@RequestMapping("/reportIncident")
	public String getNewForm(@ModelAttribute("Incident") Incident incident, BindingResult result, Model model){
		System.out.println(incident.getLocation());
		System.out.println(incident.getDescription());
		model.addAttribute("addedIncident", incident);
		if(result.hasErrors()){
			return "/incident/reportIncident";
		}
		
		return "/incident/reportIncident";
	}
}
