package ee.itcollege.ei377.iseseisevtoo3;

import java.util.List;

import ee.itcollege.ei377.iseseisevtoo3.models.Incident;

public interface IncidentService {

	List<Incident> getUnresolvedIncidents();
	
}
