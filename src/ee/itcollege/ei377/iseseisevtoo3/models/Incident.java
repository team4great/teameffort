package ee.itcollege.ei377.iseseisevtoo3.models;

import java.util.Date;
import java.util.List;


import ee.itcollege.ei377.iseseisevtoo3.Impl.IncidentSimulatorServiceImpl;


public class Incident {

	public Incident()
	{
	
	}
	
	private Date startDate;
	private Date endDate;
	private String location;
	private String description;
	private int involvedGuardCount;
	private String status;
	
	private List<Incident> getAllIncidents;
	
	public List<Incident> getGetAllIncidents() {
		if(this.getAllIncidents == null)
		{
			IncidentSimulatorServiceImpl simulatorServiceImpl = new IncidentSimulatorServiceImpl();
			this.setGetAllIncidents(simulatorServiceImpl.getUnresolvedIncidents());
		}
		
		return getAllIncidents;
	}
	private void setGetAllIncidents(List<Incident> getAllIncidents) {
		this.getAllIncidents = getAllIncidents;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getInvolvedGuardCount() {
		return involvedGuardCount;
	}
	public void setInvolvedGuardCount(int involvedGuardCount) {
		this.involvedGuardCount = involvedGuardCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
