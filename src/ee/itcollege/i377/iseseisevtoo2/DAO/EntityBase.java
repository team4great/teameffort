package ee.itcollege.i377.iseseisevtoo2.DAO;

import java.io.Serializable;

import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class EntityBase implements Serializable {

	public static final String FIND_ALL = "Entity.findAll";

	private static final long serialVersionUID = 1L;

}
