package ee.itcollege.i377.iseseisevtoo2.DAO;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the GUARD database table.
 * 
 */
@Entity
@Table(name = "GUARD")
@NamedQueries({
		@NamedQuery(name = GuardDAO.FIND_ALL, query = "select g from GuardDAO g"),
		@NamedQuery(name = GuardDAO.FIND_BY_NAME, query = "select g from GuardDAO g where g.name  = :name"),
		@NamedQuery(name = GuardDAO.FIND_BY_ID, query = "select g from GuardDAO g where g.id = :id"),
		@NamedQuery(name = GuardDAO.FIND_BY_AGE, query = "select g from GuardDAO g where g.age = :age") })
public class GuardDAO extends EntityBase {

	public static final String FIND_BY_NAME = "GuardDAO.findByName";

	public static final String FIND_BY_AGE = "GuardDAO.findByAge";

	public static final String FIND_BY_ID = "GuardDAO.findById";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int age;

	private String name;

	public GuardDAO() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}