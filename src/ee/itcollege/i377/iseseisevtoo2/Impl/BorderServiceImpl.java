package ee.itcollege.i377.iseseisevtoo2.Impl;

import java.util.List;

import ee.itcollege.i377.iseseisevtoo2.BO.GuardBO;
import ee.itcollege.i377.iseseisevtoo2.Service.BorderService;

public class BorderServiceImpl implements BorderService {

	public BorderServiceImpl() {

	}

	@Override
	public GuardBO GetGuardById(Integer Id) {

		return new GuardBO(Id);

	}

	@Override
	public GuardBO GetGuardByName(String Name) {

		return null;
	}

	@Override
	public GuardBO UpdateGuard(Integer Id, String Name) {

		return null;
	}

	@Override
	public List<GuardBO> GetAllGuards() {

		return null;
	}

	@Override
	public List<GuardBO> GetAllGuards(Integer Count) {

		return null;
	}

	@Override
	public List<GuardBO> GetAllGuardsByName(String Name) {

		return null;
	}

	@Override
	public List<GuardBO> GetAllGuardsByAge(Integer Age) {

		return null;
	}

	@Override
	public GuardBO GetGuardByNameAndAge(String Name, Integer Age) {
		// TODO Auto-generated method stub
		return null;
	}

}
