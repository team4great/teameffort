package ee.itcollege.i377.iseseisevtoo2.Impl;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ee.itcollege.i377.iseseisevtoo2.DAO.GuardDAO;
import ee.itcollege.i377.iseseisevtoo2.Service.JPAPersistenceServiceBase;

public class GuardPersistanceService extends
		JPAPersistenceServiceBase<Integer, GuardDAO> {

	public GuardDAO findById(Integer Id) {

		return (GuardDAO) em.createNamedQuery(GuardDAO.FIND_BY_ID)
				.setParameter("id", Id);

	}

}
