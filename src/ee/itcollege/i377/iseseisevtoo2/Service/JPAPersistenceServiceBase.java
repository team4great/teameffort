package ee.itcollege.i377.iseseisevtoo2.Service;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import ee.itcollege.i377.iseseisevtoo2.DAO.EntityBase;
import ee.itcollege.i377.iseseisevtoo2.DAO.GuardDAO;

public class JPAPersistenceServiceBase<K, E extends EntityBase> {
	protected Class<E> entityClass;

	@PersistenceUnit
	private EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("Team4Great");
	@PersistenceContext
	protected EntityManager em = emf.createEntityManager();

	@PostConstruct
	public void init() {

		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass
				.getActualTypeArguments()[1];
	}

	public E save(final E entity) {
		em.persist(entity);
		return entity;
	}

	public E update(final E entity) {
		return em.merge(entity);
	}

	public void remove(final E entity) {
		em.remove(em.merge(entity));
	}

	public E findById(final K id) {
		return em.find(entityClass, id);
	}

	public List<E> findAll() {
		return em.createNamedQuery(EntityBase.FIND_ALL).getResultList();
	}

}
