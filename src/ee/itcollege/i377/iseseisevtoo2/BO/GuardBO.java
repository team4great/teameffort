package ee.itcollege.i377.iseseisevtoo2.BO;

import ee.itcollege.i377.iseseisevtoo2.DAO.GuardDAO;
import ee.itcollege.i377.iseseisevtoo2.Impl.GuardPersistanceService;

public class GuardBO {

	GuardPersistanceService guardPersistanceService;

	public GuardBO(Integer id) {
		guardPersistanceService = new GuardPersistanceService();

		GuardDAO guardEntity = guardPersistanceService.findById(id);

		setName(guardEntity.getName());
		setAge(guardEntity.getAge());

	}

	protected String Name;

	protected Integer Age;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Integer getAge() {
		return Age;
	}

	public void setAge(Integer age) {
		Age = age;
	}
}
