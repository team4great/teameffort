package ee.itcollege.i377.iseseisevtoo1.BO;

public class Person {

	public Person() {
	}

	public Person(String name, String gender, Integer age) {
		this.Name = name;
		this.Gender = gender;
		this.Age = age;
	}

	protected String Name;
	protected String Gender;
	protected Integer Age;

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return Gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		Gender = gender;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return Age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(Integer age) {
		Age = age;
	}

}
