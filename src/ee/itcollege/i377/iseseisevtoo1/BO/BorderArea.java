package ee.itcollege.i377.iseseisevtoo1.BO;

public class BorderArea {

	public BorderArea() {
	}

	public BorderArea(String areaName, String areaAddress, Integer unitCount) {

		this.areaName = areaName;
		this.address = areaAddress;
		this.unitCount = unitCount;
	}

	private String areaName;
	private String address;
	private Integer unitCount;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getUnitCount() {
		return unitCount;
	}

	public void setUnitCount(Integer unitCount) {
		this.unitCount = unitCount;
	}
}
