package ee.itcollege.i377.iseseisevtoo1.BO;

public class BorderGuard extends Person {

	public BorderGuard() {

	}

	public BorderGuard(String name, String gender, Integer age) {
		super(name, gender, age);
		this.Name = name;
		this.Gender = gender;
		this.Age = age;
	}

}
