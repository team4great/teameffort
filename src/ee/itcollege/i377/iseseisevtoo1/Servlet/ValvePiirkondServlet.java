package ee.itcollege.i377.iseseisevtoo1.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ee.itcollege.i377.iseseisevtoo1.BO.BorderArea;
import ee.itcollege.i377.iseseisevtoo1.Impl.BorderServiceImpl;
import ee.itcollege.i377.iseseisevtoo1.Service.BorderService;

@WebServlet("/ValvePiirkond")
public class ValvePiirkondServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpSession runningSession;
	BorderService borderService = new BorderServiceImpl();

	public ValvePiirkondServlet() {
		super();

	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String inputAreaName = request.getParameter("areaName");
		String inputAddress = request.getParameter("address");
		String parameterUnitCount = request.getParameter("unitCount");

		if (parameterUnitCount != null) {
			Integer inputUnitCount = Integer.parseInt(parameterUnitCount);
			BorderArea inputBorderArea = new BorderArea(inputAreaName,
					inputAddress, inputUnitCount);
			request.setAttribute("BorderArea", inputBorderArea);
		}

		request.getRequestDispatcher("DisplayBorderAreaInfo.jsp").forward(
				request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		this.runningSession = request.getSession(true);
		List<BorderArea> borderAreasInHttpSession;

		if (this.runningSession.getAttribute("Areas") != null) {
			borderAreasInHttpSession = (List<BorderArea>) this.runningSession
					.getAttribute("Areas");
		} else {
			borderAreasInHttpSession = this.borderService.GetAllBorderdAreas();
		}

		String inputAreaName = (String) request.getParameter("areaName");
		String inputAddress = (String) request.getParameter("address");
		String parameterUnitCount = request.getParameter("unitCount");

		if (parameterUnitCount != null) {
			Integer inputUnitCount = Integer.parseInt(parameterUnitCount);
			BorderArea inputBorderArea = new BorderArea(inputAreaName,
					inputAddress, inputUnitCount);
			borderAreasInHttpSession.add(inputBorderArea);
		}

		this.runningSession.setAttribute("Areas", borderAreasInHttpSession);

		request.setAttribute("BorderAreasInSession", borderAreasInHttpSession);
		request.getRequestDispatcher("BorderAreaTable.jsp").forward(request,
				response);
	}

}
