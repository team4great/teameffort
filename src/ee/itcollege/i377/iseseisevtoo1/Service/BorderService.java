package ee.itcollege.i377.iseseisevtoo1.Service;

import java.util.List;

import ee.itcollege.i377.iseseisevtoo1.BO.BorderArea;

public interface BorderService {

	public List<BorderArea> GetAllBorderdAreas();

	public BorderArea GetBorderAreaByAreaName(String areaName);
}
