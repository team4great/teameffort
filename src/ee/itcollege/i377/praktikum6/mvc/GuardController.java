package ee.itcollege.i377.praktikum6.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ee.itcollege.i377.praktikum6.models.Guard;

@Controller
public class GuardController {

	@RequestMapping("/guardForm")
	public String guardForm(@ModelAttribute Guard formInput, Model model) {
		System.out.println(formInput.getAge());
		model.addAttribute("addedGuard", formInput.getAge());
		return "Guard";
	}
}
