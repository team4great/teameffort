package ee.itcollege.i377.praktikum6.mvc;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ee.itcollege.i377.praktikum6.models.TimeModel;

@Controller
public class TimeController {

	@RequestMapping(method = RequestMethod.GET, value = "/currentTime")
	@ResponseBody
	public String currentTime(
			@RequestParam(required = false) String timeInMillis, TimeModel model) {
		return model.getCurrentTime();
	}

	@RequestMapping(value = "/timeTillTheEnd", method = RequestMethod.GET)
	public String printTimeTillTheEnd(Model model) {
		model.addAttribute("timeTillTheEnd", new Date(4449546671000L));
		return "Time";
	}
}
