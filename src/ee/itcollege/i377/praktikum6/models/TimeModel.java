package ee.itcollege.i377.praktikum6.models;

import java.util.Date;

public class TimeModel {

	public TimeModel() {
		setCurrentTime(new java.util.Date().toString());
	}

	public TimeModel(String timeInMillis) {
		try {
			Long time = Long.parseLong(timeInMillis);
			setCurrentTime(new java.util.Date(time).toString());
		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
		}
		currentTime = "Error";

	}

	private String currentTime;

	public String getCurrentTime() {
		return this.currentTime;
	}

	public void setCurrentTime(String CurrentTime) {
		this.currentTime = CurrentTime;
	}

	public void addAttribute(String string, Date date) {
		// TODO Auto-generated method stub

	}

}
